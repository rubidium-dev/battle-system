extern crate rand;

mod entity;
mod systems;
mod templates;

use std::io::{self, Write};
use entity::{Battle, BattleState, Entity, Natures, QueuedCommand};
use templates::*;

fn main() {
    let mut battle = Battle::new(
        Entity::from_template(&DEX_025, 52, Natures::Calm),
        Entity::from_template(&DEX_025, 50, Natures::Modest),
    );
    battle.ally.moves[0] = Some(templates::moves::QUICK_ATTACK);
    battle.enemy.moves[0] = Some(templates::moves::THUNDER_SHOCK);

    let mut output = io::stdout();
    while battle.state != BattleState::End {
        battle.next(&mut output).expect("I/O error");
        if battle.state == BattleState::Command && battle.ally_command.is_none() {
            println!("\n----");
            println!("Round {}", battle.round);
            println!("Your {}: {} hp", battle.ally.dex_no, battle.ally.transient.hp);
            println!("Enemy {}: {} hp\n", battle.enemy.dex_no, battle.enemy.transient.hp);
            for (i, m) in battle.ally.moves.iter().enumerate() {
                if m.is_some() {
                    println!("{}) {}", i + 1, m.unwrap().name);
                }
            }
            println!("5) Run");
            print!("Choose a command: ");
            output.flush().expect("Unable to write to stdout");
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Unable to read from stdin");
            match input.trim() {
                "1" if battle.ally.moves[0].is_some() => battle.ally_command = Some(QueuedCommand::UseMove(battle.ally.moves[0].unwrap())),
                "2" if battle.ally.moves[1].is_some() => battle.ally_command = Some(QueuedCommand::UseMove(battle.ally.moves[1].unwrap())),
                "3" if battle.ally.moves[2].is_some() => battle.ally_command = Some(QueuedCommand::UseMove(battle.ally.moves[2].unwrap())),
                "4" if battle.ally.moves[3].is_some() => battle.ally_command = Some(QueuedCommand::UseMove(battle.ally.moves[3].unwrap())),
                "5" => battle.ally_command = Some(QueuedCommand::Run),
                _ => println!("Unknown command."),
            }
            println!("");
        }
    }
}
