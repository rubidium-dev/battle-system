use crate::systems;
pub use components::*;

/// Tracks the current highest unique entity identifer.
static mut CURRENT_ENTITY_ID: usize = 0;

/// Maximum number of moves a battler can have.
pub const MAX_MOVES: usize = 4;

/// Since this prototype is only focused on battle stats, the base entity
/// represents everything needed for a battler to participate in combat.
#[derive(Debug)]
pub struct Entity {
    /// Unique identifier for this battler.
    pub entity_id: usize,
    /// Dex #, for reference.
    pub dex_no: i32,
    /// Entity elemental types.
    pub types: EntityTypes,
    /// Experience level for this battler.
    pub level: i32,
    /// Nature of this battler.
    pub nature: Natures,
    /// Battle statistics for this battler.
    pub stats: BattleStats,
    /// Transient battle statistics for this battler.
    pub transient: TransientStats,
    /// This battler's moveset.
    pub moves: [Option<Move>; MAX_MOVES],
}

impl Entity {
    /// Creates a new entity from the given `template` at the given experience `level`.
    pub fn from_template(template: &EntityTemplate, level: i32, nature: Natures) -> Self {
        let mut battler = unsafe {
            // use of static mut is fine, I don't plan to multithread this prototype
            CURRENT_ENTITY_ID += 1;
            Entity {
                entity_id: CURRENT_ENTITY_ID,
                dex_no: template.dex_no,
                types: template.types,
                level: 0,
                nature,
                stats: template.stats,
                transient: TransientStats {
                    hp: 0,
                    acc_stages: 0,
                    eva_stages: 0,
                    atk_stages: 0,
                    def_stages: 0,
                    sp_atk_stages: 0,
                    sp_def_stages: 0,
                    spd_stages: 0,
                    escape_attempts: 0,
                },
                moves: [None; MAX_MOVES],
            }
        };

        // TODO: implement IVs/EVs? just assume they're all maxed for now
        let iv = BattleStats {
            hp: 31,
            atk: 31,
            def: 31,
            sp_atk: 31,
            sp_def: 31,
            spd: 31,
        };
        let ev = BattleStats {
            hp: 255,
            atk: 255,
            def: 255,
            sp_atk: 255,
            sp_def: 255,
            spd: 255,
        };
        systems::stats::adjust_stats_to_level(&mut battler, level, &iv, &ev);

        battler
    }
}

/// A template for initializing battler entities. Basically this represents
/// the stats as declared in the dex.
#[derive(Debug)]
pub struct EntityTemplate {
    /// Dex #.
    pub dex_no: i32,
    /// Entity elemental types.
    pub types: EntityTypes,
    /// Base battle statistics at level 50.
    pub stats: BattleStats,
}

/// State tracking for a `Battle`.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum BattleState {
    Start,
    Command,
    BeforeMoves,
    Resolve(i32),
    AfterMoves,
    End,
}

/// State of the current battle between two entities.
#[derive(Debug)]
pub struct Battle {
    /// The ally battler, controlled by the player.
    pub ally: Entity,
    /// The enemy battler, controlled by AI.
    pub enemy: Entity,
    /// Round counter.
    pub round: i32,
    /// Current state.
    pub state: BattleState,
    /// Command selected by the player.
    pub ally_command: Option<QueuedCommand>,
    /// Command selected by the AI.
    pub enemy_command: Option<QueuedCommand>,
}

impl Battle {
    /// Creates a new `Battle` around the entities for the `ally` and `enemy` battlers.
    pub fn new(ally: Entity, enemy: Entity) -> Self {
        Battle {
            ally,
            enemy,
            round: 1,
            state: BattleState::Start,
            ally_command: None,
            enemy_command: None,
        }
    }
}

mod components {
    /// Possible elemental types for battlers and moves.
    #[derive(Clone, Copy, Debug)]
    #[allow(dead_code)]
    pub enum Types {
        Normal,
        Bug,
        Dark,
        Dragon,
        Electric,
        Fairy,
        Fighting,
        Fire,
        Flying,
        Ghost,
        Grass,
        Ground,
        Ice,
        Poison,
        Psychic,
        Rock,
        Steel,
        Water,
    }

    /// Possible natures for battlers (affects stat growth).
    #[derive(Clone, Copy, Debug)]
    #[allow(dead_code)]
    pub enum Natures {
        Hardy,
        Lonely,
        Brave,
        Adamant,
        Naughty,
        Bold,
        Docile,
        Relaxed,
        Impish,
        Lax,
        Timid,
        Hasty,
        Serious,
        Jolly,
        Naive,
        Modest,
        Mild,
        Quiet,
        Bashful,
        Rash,
        Calm,
        Gentle,
        Sassy,
        Careful,
        Quirky,
    }

    /// Component holding the primary/secondary types for an entity.
    #[derive(Clone, Copy, Debug)]
    pub struct EntityTypes {
        /// Primary type.
        pub primary: Types,
        /// Secondary type (if any).
        pub secondary: Option<Types>,
    }

    /// Component holding battle statistics for an entity.
    #[derive(Clone, Copy, Debug)]
    pub struct BattleStats {
        /// Hit points (HP) determine how much damage the entity can take before fainting.
        pub hp: i32,
        /// Attack (Atk) partly determines how much damage is dealt by physical moves.
        pub atk: i32,
        /// Defense (Def) partly determines how much damage is received from physical moves.
        pub def: i32,
        /// Special attack (Sp. Atk) partly determines how much damage is dealt by special moves.
        pub sp_atk: i32,
        /// Special defense (Sp. Def) partly determines how much damage is received from special moves.
        pub sp_def: i32,
        /// Speed (Spd) determines the order that the entity acts in battle.
        pub spd: i32,
    }

    /// Component holding tranisent statistics for in-battle adjustments.
    #[derive(Clone, Copy, Debug)]
    pub struct TransientStats {
        /// Current hit points.
        pub hp: i32,
        /// Accuracy (Acc) determines the probability of hitting with a move.
        pub acc_stages: i32,
        /// Evasion or Evasiveness (Eva) determines the probability of avoiding moves.
        pub eva_stages: i32,
        /// Sliding scale for attack, from -6 to +6.
        pub atk_stages: i32,
        /// Sliding scale for defense, from -6 to +6.
        pub def_stages: i32,
        /// Sliding scale for special attack, from -6 to +6.
        pub sp_atk_stages: i32,
        /// Sliding scale for special defense, from -6 to +6.
        pub sp_def_stages: i32,
        /// Sliding scale for speed, from -6 to +6.
        pub spd_stages: i32,
        /// Number of times the battler has attempted to flee.
        pub escape_attempts: i32,
    }

    impl TransientStats {
        /// Resets this set of transient stats back to baseline.
        pub fn reset(&mut self, max_hp: i32) {
            self.hp = max_hp;
            self.acc_stages = 0;
            self.eva_stages = 0;
            self.atk_stages = 0;
            self.def_stages = 0;
            self.sp_atk_stages = 0;
            self.sp_def_stages = 0;
            self.spd_stages = 0;
            self.escape_attempts = 0;
        }
    }

    /// Component holding a queued command for a battler.
    #[derive(Debug)]
    #[allow(dead_code)]
    pub enum QueuedCommand {
        Switch,
        UseMove(Move),
        UseItem,
        Run,
    }

    /// Stores whether a move is a physical attack or special attack.
    #[derive(Clone, Copy, Debug)]
    pub enum MoveCategory {
        Physical,
        Special,
    }

    #[derive(Clone, Copy, Debug)]
    pub struct Move {
        /// The move's elemental type.
        pub move_type: Types,
        /// Whether the move is physical or special,
        pub category: MoveCategory,
        /// The move's power points (number of times it can be used).
        pub pp: i32,
        /// The move's power (damage), if any.
        pub power: Option<i32>,
        /// The move's accuracy. If None, it cannot miss.
        pub accuracy: Option<i32>,
        /// The move's priority during a round.
        pub priority: i32,
        pub name: &'static str,
    }
}
