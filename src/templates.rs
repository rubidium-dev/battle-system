use crate::entity::{BattleStats, EntityTemplate, EntityTypes, Types};

pub static DEX_025: EntityTemplate = EntityTemplate {
    dex_no: 25,
    types: EntityTypes {
        primary: Types::Electric,
        secondary: None,
    },
    stats: BattleStats {
        hp: 35,
        atk: 55,
        def: 40,
        sp_atk: 50,
        sp_def: 50,
        spd: 90,
    },
};

pub mod moves {
    use crate::entity::{Move, MoveCategory, Types};

    pub static QUICK_ATTACK: Move = Move {
        move_type: Types::Normal,
        category: MoveCategory::Physical,
        pp: 30,
        power: Some(40),
        accuracy: Some(100),
        priority: 1,
        name: "Quick Attack",
    };

    pub static THUNDER_SHOCK: Move = Move {
        move_type: Types::Electric,
        category: MoveCategory::Special,
        pp: 30,
        power: Some(40),
        accuracy: Some(100),
        priority: 0,
        name: "Thunder Shock",
    };
}