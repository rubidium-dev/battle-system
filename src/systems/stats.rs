use crate::entity::*;

/// Adjusts this entity's base stats to the given experience `level`.
/// `iv` and `ev` represent the IVs and EVs that complement those stats.
pub fn adjust_stats_to_level(entity: &mut Entity, level: i32, iv: &BattleStats, ev: &BattleStats) {
    assert!(level > 0 && level < 101, "Level must be from 1 to 100.");
    assert!(
        entity.level == 0,
        "Calling adjust_stats_to_level multiple times results in meaningless data."
    );

    // core calculation
    const CORE: fn(i32, i32, i32, i32) -> i32 =
        |base, iv, ev, level| ((2 * base) + iv + (ev / 4)) * level / 100;

    entity.level = level;
    entity.stats.hp = CORE(entity.stats.hp, iv.hp, ev.hp, level) + level + 10;
    entity.stats.atk = CORE(entity.stats.atk, iv.atk, ev.atk, level) + 5;
    entity.stats.atk = entity.nature.adj_atk(entity.stats.atk);

    entity.stats.def = CORE(entity.stats.def, iv.def, ev.def, level) + 5;
    entity.stats.def = entity.nature.adj_def(entity.stats.def);

    entity.stats.sp_atk = CORE(entity.stats.sp_atk, iv.sp_atk, ev.sp_atk, level) + 5;
    entity.stats.sp_atk = entity.nature.adj_sp_atk(entity.stats.sp_atk);

    entity.stats.sp_def = CORE(entity.stats.sp_def, iv.sp_def, ev.sp_def, level) + 5;
    entity.stats.sp_def = entity.nature.adj_sp_def(entity.stats.sp_def);

    entity.stats.spd = CORE(entity.stats.spd, iv.spd, ev.spd, level) + 5;
    entity.stats.spd = entity.nature.adj_spd(entity.stats.spd);
}

impl Natures {
    fn adj_atk(&self, atk: i32) -> i32 {
        match self {
            Natures::Lonely | Natures::Brave | Natures::Adamant | Natures::Naughty => atk * 110 / 100,
            Natures::Bold | Natures::Timid | Natures::Modest | Natures::Calm => atk * 90 / 100,
            _ => atk,
        }
    }

    fn adj_def(&self, def: i32) -> i32 {
        match self {
            Natures::Bold | Natures::Relaxed | Natures::Impish | Natures::Lax => def * 110 / 100,
            Natures::Lonely | Natures::Hasty | Natures::Mild | Natures::Gentle => def * 90 / 100,
            _ => def,
        }
    }

    fn adj_sp_atk(&self, sp_atk: i32) -> i32 {
        match self {
            Natures::Modest | Natures::Mild | Natures::Quiet | Natures::Rash => sp_atk * 110 / 100,
            Natures::Adamant | Natures::Impish | Natures::Jolly | Natures::Careful => sp_atk * 90 / 100,
            _ => sp_atk,
        }
    }

    fn adj_sp_def(&self, sp_def: i32) -> i32 {
        match self {
            Natures::Calm | Natures::Gentle | Natures::Sassy | Natures::Careful => sp_def * 110 / 100,
            Natures::Naughty | Natures::Lax | Natures::Naive | Natures::Rash => sp_def * 90 / 100,
            _ => sp_def,
        }
    }

    fn adj_spd(&self, spd: i32) -> i32 {
        match self {
            Natures::Timid | Natures::Hasty | Natures::Jolly | Natures::Naive => spd * 110 / 100,
            Natures::Brave | Natures::Relaxed | Natures::Quiet | Natures::Sassy => spd * 90 / 100,
            _ => spd,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn adjust_stats_to_level_ok() {
        // based on example from https://bulbapedia.bulbagarden.net/wiki/Statistic
        let mut ent = Entity {
            entity_id: 0,
            dex_no: 0,
            types: EntityTypes {
                primary: Types::Normal,
                secondary: None,
            },
            level: 0,
            nature: Natures::Adamant,
            stats: BattleStats {
                hp: 108,
                atk: 130,
                def: 95,
                sp_atk: 80,
                sp_def: 85,
                spd: 102,
            },
            transient: TransientStats {
                hp: 0,
                acc_stages: 0,
                eva_stages: 0,
                atk_stages: 0,
                def_stages: 0,
                sp_atk_stages: 0,
                sp_def_stages: 0,
                spd_stages: 0,
                escape_attempts: 0,
            },
            moves: [None; crate::entity::MAX_MOVES],
        };
        let iv = BattleStats {
            hp: 24,
            atk: 12,
            def: 30,
            sp_atk: 16,
            sp_def: 23,
            spd: 5,
        };
        let ev = BattleStats {
            hp: 74,
            atk: 190,
            def: 91,
            sp_atk: 48,
            sp_def: 84,
            spd: 23,
        };

        adjust_stats_to_level(&mut ent, 78, &iv, &ev);
        assert_eq!(ent.level, 78);
        assert_eq!(ent.stats.hp, 289);
        assert_eq!(ent.stats.atk, 278);
        assert_eq!(ent.stats.def, 193);
        assert_eq!(ent.stats.sp_atk, 135);
        assert_eq!(ent.stats.sp_def, 171);
        assert_eq!(ent.stats.spd, 171);
    }
}
