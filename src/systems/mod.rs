use crate::entity::*;
use rand::prelude::*;
use std::io::Write;

pub mod stats;

// See https://bulbapedia.bulbagarden.net/wiki/Priority
const PRIORITY_TOP: i32 = 5;
const PRIORITY_BOTTOM: i32 = -7;

impl Battle {
    /// Attempts to process the current battle state and move to the next one.
    pub fn next<T>(&mut self, mut msg_receiver: T) -> Result<(), std::io::Error>
        where T: Write {
        self.check_battle_over();
        // TODO: determine who gets to act first
        // TODO: more state implementation
        match self.state {
            BattleState::Start => {
                self.ally.transient.reset(self.ally.stats.hp);
                self.enemy.transient.reset(self.enemy.stats.hp);
                self.ally_command = None;
                self.enemy_command = None;
                self.state = BattleState::Command;
            }
            BattleState::Command => {
                if self.ally_command.is_some() && self.enemy_command.is_some() {
                    self.state = BattleState::BeforeMoves;
                } else if self.enemy_command.is_none() {
                    self.select_enemy_command();
                }
            }
            BattleState::BeforeMoves => {
                self.state = BattleState::Resolve(PRIORITY_TOP);

                // check for commands that don't have priority
                match self.ally_command {
                    Some(QueuedCommand::Run) => {
                        self.ally.transient.escape_attempts += 1;
                        if escape_from_battle(self.ally.transient.escape_attempts, self.ally.stats.spd, self.enemy.stats.spd) {
                            writeln!(msg_receiver, "You escaped!")?;
                            self.state = BattleState::End;
                            return Ok(());
                        } else {
                            writeln!(msg_receiver, "You are unable to escape!")?;
                        }
                    }
                    Some(QueuedCommand::Switch) => unimplemented!("Can't switch battlers"),
                    Some(QueuedCommand::UseItem) => unimplemented!("Can't use items"),
                    _ => {}
                }
                match self.enemy_command {
                    Some(QueuedCommand::Run) => {
                        self.enemy.transient.escape_attempts += 1;
                        if escape_from_battle(self.enemy.transient.escape_attempts, self.enemy.stats.spd, self.ally.stats.spd) {
                            writeln!(msg_receiver, "The {} escaped!", self.enemy.dex_no)?;
                            self.state = BattleState::End;
                            return Ok(());
                        } else {
                            writeln!(msg_receiver, "The {} was unable to escape!", self.enemy.dex_no)?;
                        }
                    },
                    Some(QueuedCommand::Switch) => unimplemented!("Can't switch battlers"),
                    Some(QueuedCommand::UseItem) => unimplemented!("Can't use items"),
                    _ => {}
                }
            }
            BattleState::Resolve(priority) if priority >= PRIORITY_BOTTOM => {
                match self.ally_command {
                    Some(QueuedCommand::UseMove(m)) if m.priority == priority => {
                        // TODO
                        writeln!(msg_receiver, "Your {} used {}!", self.ally.dex_no, m.name)?;
                    }
                    _ => {}
                }
                match self.enemy_command {
                    Some(QueuedCommand::UseMove(m)) if m.priority == priority => {
                        // TODO
                        writeln!(msg_receiver, "The enemy {} used {}!", self.enemy.dex_no, m.name)?;
                    }
                    _ => {}
                }
                self.state = BattleState::Resolve(priority - 1);
            }
            BattleState::Resolve(_) => {
                self.state = BattleState::AfterMoves;
            }
            BattleState::AfterMoves => {
                self.ally_command = None;
                self.enemy_command = None;
                self.round += 1;
                self.state = BattleState::Command;
            }
            BattleState::End => {}
        }

        Ok(())
    }

    fn check_battle_over(&mut self) {
        if self.ally.stats.hp < 1 || self.enemy.stats.hp < 1 {
            self.state = BattleState::End;
        }
    }

    fn select_enemy_command(&mut self) {
        // TODO
        if self.enemy.moves[0].is_some() {
            self.enemy_command = Some(QueuedCommand::UseMove(self.enemy.moves[0].unwrap()));
        } else {
            self.enemy_command = Some(QueuedCommand::Run);
        }
    }
}

fn escape_from_battle(attempt_number: i32, attempting_spd: i32, opponent_spd: i32) -> bool {
    // Source: https://bulbapedia.bulbagarden.net/wiki/Escape
    let probability = (((attempting_spd * 128) / opponent_spd) + 30 * attempt_number) % 256;
    let result = rand::thread_rng().gen_range(0, 256);
    result < probability
}
