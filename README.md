# Battle System
This is a simple implementation and test harness for a Pokémon battle system. The intent
is just to theorize how I would adapt a complete system from a written spec. (I'm using
mostly the descriptions on [Bulbapedia](https://bulbapedia.bulbagarden.net/) for reference.)
The example utilizes a very simplistic entity-component-system pattern tailored for
this program.

Obviously, this has no affiliation with official Pokémon products and makes no use of
copyrighted material from the games. Pokémon® is a registered trademark of Nintendo.